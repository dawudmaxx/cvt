# set path
$env:Path = "C:\MinGW\bin;" + "C:\cmake-3.26.0-windows-x86_64\bin;" + $env:Path
# create build dir and cd into it
if (-not (Test-Path -Path build_mingw)) {
    mkdir build_mingw
}
Set-Location -Path "build_mingw"
# configure and generate the make files
cmake .. -G "MinGW Makefiles"
# build the cmake targets
mingw32-make