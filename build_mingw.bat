:: set path
set PATH=%PATH%;C:\MinGW\bin;C:\cmake-3.26.0-windows-x86_64\bin
:: create build dir and cd into it
if not exist build_mingw mkdir build_mingw
cd build_mingw
:: configure and generate the make files
cmake .. -G "MinGW Makefiles"
:: build the cmake targets
mingw32-make