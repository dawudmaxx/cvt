#include "ParticleFilter.h"

#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

//You must use your own path here!
static const string MAP_FILE_PATH = string(ProjectDir) + string("/Map.bmp");
static const string MEASUREMENT_FILE_PATH = string(ProjectDir) + string("/measurements.txt");

//#define _CREATE_YOUR_OWN_MEASUREMENT_FILE_ _CREATE_YOUR_OWN_MEASUREMENT_FILE_

void createMeasurementFile(const char *filename, Robot &robot)
{
    FILE *f{nullptr};

    f = fopen(filename, "w");

    if (!f)
    {
        return;
    }

    double dx = 10.0;
    for (double x = robot.x(); x < 450; x += dx)
    {
        robot.move(dx, 0.0, 0.0);
        const double *measurement = robot.perceiveEnvironment();
        fprintf(f, "%lf %lf %lf %lf %lf %lf ", robot.x(), robot.y(), robot.yaw(), dx, 0.0, 0.0);
        for (uint32_t i = 0; i < robot.totalNumberOfMeasurements(); i++)
        {
            fprintf(f, "%lf ", measurement[i]);
        }

        fprintf(f, "\n");
    }

    fclose(f);
}

/*
*Use this method to load the measurement file and read the measurements line by line.
*For every sensor reading call the particle filter to update the posterior.
*@param filename the path to the file containing the measurements
*@param filter a reference to the particle filter
*@param sensorPtr a pointer to the sensor; use this if required (e.g. to get the number of measurements etc.)
*/
void executeParticleFilter(const char *filename, ParticleFilter &filter, std::shared_ptr<Sensor> sensorPtr)
{

    ////////////////////////////////////////////////////////////////////////////
    //////////YOUR CODE GOES HERE!!!////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
	fstream myfile;
	myfile.open(string(filename));
	string line;
	if (myfile.is_open())
	{
		double dx, dy, dyaw;
		vector<double> measurement_vec;		
		while (getline(myfile, line))
		{
			stringstream ss(line);			
			string word;
			ss >> word; // robot.x()
			ss >> word; // robot.y()
			ss >> word; // robot.yaw()
			ss >> word;
			dx = atof(word.c_str());
			ss >> word;
			dy = atof(word.c_str());
			ss >> word;
			dyaw = atof(word.c_str());
			while (ss >> word)
				measurement_vec.push_back(atof(word.c_str()));
			const double *measurement = measurement_vec.data();
			// call the PF to update the posterior
			filter.filter(dx, dy, dyaw, measurement, static_cast<uint32_t>(measurement_vec.size()));
			measurement_vec.clear();
		}
		myfile.close();
	}
}

int main()
{
    std::shared_ptr<Map> mapPtr = std::make_shared<Map>();

    mapPtr->init(MAP_FILE_PATH.c_str());
	
    std::shared_ptr<Sensor> sensorPtr = std::make_shared<Sensor>(mapPtr);

#ifdef _CREATE_YOUR_OWN_MEASUREMENT_FILE_

    Robot robot;
    robot.init(sensorPtr, 113.0, 114.0, 0.0);

	createMeasurementFile(MEASUREMENT_FILE_PATH, robot);

#endif

    ParticleFilter filter;
    filter.init(sensorPtr, mapPtr, 10000);

    executeParticleFilter(MEASUREMENT_FILE_PATH.c_str(), filter, sensorPtr);
}
