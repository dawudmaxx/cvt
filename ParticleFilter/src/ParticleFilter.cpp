#include "ParticleFilter.h"
#include <time.h>
#include <random>
#include <cstring>

#include <iostream>
#include <numeric>

using namespace std;

void ParticleFilter::init(std::shared_ptr<Sensor> sensor, std::shared_ptr<Map> map, uint32_t numberOfParticles)
{
    _filterIteration = 0;
    _priorSetIndex = 0;
    _updatedSetIndex = 1;
    _particles[0].resize(numberOfParticles);
    _particles[1].resize(numberOfParticles);
    _accumulatedWeights.resize(numberOfParticles);
    _mapPtr = map;
    _sensorPtr = sensor;

	// add your code here
	_mean[0] = 0;
	_mean[1] = 0;
	_mean[1] = 0;
 
    srand((unsigned int)time(NULL));

    for (uint32_t i = 0; i < numberOfParticles; i++)
    {
        double x = (double)(rand() % _mapPtr->width());
        double y = (double)(rand() % _mapPtr->height());
        double yaw = (double)(rand() % 360) * M_PI / 180.0;
        _particles[_priorSetIndex][i].init(sensor, x, y, yaw);
        _particles[_updatedSetIndex][i].init(sensor, x, y, yaw);
    }

    //_particles[_priorSetIndex][0].setPose(113.0, 114.0, 0);

    _dbgImgHandler = *map;
}

void ParticleFilter::filter(double dx, double dy, double dYaw, const double *measurement, uint32_t length)
{
	// step 1: compute the priors using the proces model
	normal_distribution<> normal_x{ _mean[0], stdDevX };
	normal_distribution<> normal_y{ _mean[1], stdDevY };
	normal_distribution<> normal_yaw{ _mean[2], stdDevYaw };

	ParticleSet new_particle_set;
	_particles[_priorSetIndex] = ParticleSet(_particles[_updatedSetIndex]);	

	for (auto& particle : _particles[_priorSetIndex])
	{
		dx = dx + normal_x(_mersenne);
		dy = dy + normal_y(_mersenne);
		dYaw = dYaw + normal_yaw(_mersenne);

		particle.move(dx, dy, dYaw);

		// step 2: Measurement Step: refine the estimate using the Bayes Theorem
		particle.computeWeight(measurement, _mapPtr);
		_accumulatedWeights.push_back(particle.weight());

		new_particle_set.push_back(particle);
	}

	// find cumulative normalized sum
	// normalize weight
	double weight_sum = accumulate(begin(_accumulatedWeights), end(_accumulatedWeights), 0.0);
	for (int i=0; i<static_cast<int>(new_particle_set.size()); i++)
	{
		new_particle_set[i].setWeight(new_particle_set[i].weight() / (weight_sum+0.01));
		if (i != 0)
			_accumulatedWeights[i] += _accumulatedWeights[i - 1];
	}
	
	// step 3: resampling
	_particles[_updatedSetIndex].clear();
	// https://blog.finxter.com/proportional-sampling-using-weighted-values/
	uniform_real_distribution<> normal_meas(0, 1.0); // unform dist. between 0 and 1
	double w = normal_meas(_mersenne);
	for (uint8_t i = 0; i<new_particle_set.size(); i++)
	{		
		if (w <= _accumulatedWeights[i])
		{
			_particles[_updatedSetIndex].push_back(new_particle_set[i]);
		}
	}

	char buff[100];
	sprintf(buff, "%s/debug_file_%d.bmp", ProjectDir, _filterIteration);
	writeParticlesToMapImage(buff);
	_filterIteration++;
}

void ParticleFilter::writeParticlesToMapImage(const char *filename)
{
    _dbgImgHandler.updateImage(_mapPtr->imageData(), _mapPtr->imageSizeInByte());

    for (uint8_t i = 0; i < _particles[_priorSetIndex].size(); i++)
    {
        uint32_t x = (uint32_t)std::round(_particles[_priorSetIndex][i].x());
        uint32_t y = (uint32_t)std::round(_particles[_priorSetIndex][i].y());
        _dbgImgHandler.updateImage(x, y, 255, 0, 0);
    }

    _dbgImgHandler.saveImageAs(filename);
}

const double *ParticleFilter::mean() const
{
    return _mean;
}