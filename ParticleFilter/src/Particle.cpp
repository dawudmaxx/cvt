#include "Particle.h"
#include <cmath>
#include <iostream>
#include <random>

using namespace std;

/*
Links
https://www.youtube.com/watch?v=rwzzEhqkt6c
https://www.youtube.com/watch?v=OM5iXrvUr_o&t=1s
https://www.youtube.com/watch?v=UOKYhuGOUPI
http://ais.informatik.uni-freiburg.de/teaching/ss19/robotics/exercises/solutions/06/sheet06sol.pdf
https://towardsdatascience.com/naive-bayes-implementation-from-scratch-using-c-51c958094041
https://blog.finxter.com/proportional-sampling-using-weighted-values/
https://www.youtube.com/watch?v=qsLmj-6AktU
https://github.com/rlabbe/Kalman-and-Bayesian-Filters-in-Python/blob/master/12-Particle-Filters.ipynb
file:///D:/DOWNLOADS/sensors-21-00438-v2.pdf
file:///D:/LITERATURE/02_MS/courses1/Computer%20Vision/10.Particle%20filter.pdf
file:///D:/LITERATURE/02_MS/courses1/Computer%20Vision/7.Bayesian%20tracking.pdf
*/

void Particle::computeWeight(const double *measurement, std::shared_ptr<Map> _mapPtr)
{
	/*
	Given: measurement, MEASUREMENT_VAR, _x, _y, _yaw
	Output: _weight
	*/
	int N = sizeof(measurement) / sizeof(double);
	double all_meas_likelihood = 1;
	for (int i = 0; i < N; i++)
	{		
		const double& meas_range = measurement[i];
		// calculate expected range measurement
		double dist_range_exp;
		_mapPtr->value(static_cast<uint32_t>(_x), static_cast<uint32_t>(_y), dist_range_exp);
		// evaluate sensor model(probability density function of normal distribution)
		const auto& meas_likelihood = exp(-0.5*pow(dist_range_exp - meas_range, 2) / MEASUREMENT_VAR);
		
		all_meas_likelihood = all_meas_likelihood * meas_likelihood;
	}
	_weight = all_meas_likelihood;
}

double Particle::weight()
{
    return _weight;
}
void Particle::setWeight(double weight)
{
    _weight = weight;
}

void Particle::setPose(double x, double y, double yaw)
{
    _x = x;
    _y = y;
    _yaw = yaw;

    if (_sensor != nullptr)
    {
        _sensor->updatePose(_x, _y, _yaw);
    }
}

Particle &Particle::operator=(Particle &p)
{
    _x = p.x();
    _y = p.y();
    _yaw = p.yaw();
    _weight = p.weight();
    return *this;
}