#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

int solution2(vector<int>& A) {
	int n = A.size();
	int i = n - 1;
	int result = -1;
	int k = 0;
	int maximal = 0;
	while (i > 0) {
		if (A[i] == 1) {
			k = k + 1;
			if (k >= maximal) {
				maximal = k;
				result = i;
			}
		}
		else {
			k = 0;
		}
		i = i - 1;
	}
	if (A[i] == 1 && k + 1 >= maximal)
		result = 0;
	return result;
}


int main8()
{
	int N = 10;
	vector<int> vec = {1,1,0,0,0,1,1,0,0,0,1,1};
	assert(N >= 1);
	assert(N <= 1000);
	
	cout << "Ans = " << solution2(vec) << endl;
	return EXIT_SUCCESS;
}