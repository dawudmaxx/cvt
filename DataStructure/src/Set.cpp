#include <Set.hpp>
using namespace std;

void printSet(std::set<int> set)
{
	std::cout << "The set_a is = ";
	for (auto i = set.begin(); i != set.end(); ++i)
	{
		std::cout << *i;
	}
	std::cout << std::endl;
}
/*
Sets are a type of associative containers in which each element has to be unique because the value of the element identifies it. 
The values are stored in a specific order.
	1. The set stores the elements in sorted order.
    2. All the elements in a set have unique values.
	3. The value of the element cannot be modified once it is added to the set, 
	though it is possible to remove and then add the modified value of that element. Thus, the values are immutable.
	4. Sets follow the Binary search tree implementation.
	5. The values in a set are unindexed.

	Note: To store the elements in an unsorted(random) order,  unordered_set() can be used.
*/

void runSet()
{
	cout << "<<< SET (items are unchangeable)>>> " << endl;
	set<int> set_a = { 7,3,1,5, 1}; // second 1 is ignored
	/*
	insert: copies or move the object 
	**emplace: object is constrcted in-line. Prefered over insert.
	count(value): returns 1 is present else 0
	find(value): returns the itr object. Useful for non-primitive datatypes.
	erase
	swap
	*/
	set_a.insert(9);
	set_a.emplace(11);
	printSet(set_a);
	cout << "print last element with end()-1" << endl;
	cout << *(--set_a.end()) << endl;

	// find and erase
	auto itr = set_a.find(3);
	cout << "FIND 3" << endl;
	cout << "REMOVE 3" << endl;
	set_a.erase(itr);

	// count
	cout << "count(1) called" << endl;
	if (set_a.count(1))
	{
		cout << "1 present (1) times" << endl; // occurence cannot be more than one
	}
	else
	{
		cout << "1 not present " << endl;
	}
	printSet(set_a);

	cout <<"find(1) called"<< endl;
	if (set_a.find(1) != set_a.end())
	{
		cout << "1 is present 1 times" << endl;
	}
	else
	{
		cout << "1 is not present" << endl;
	}

	cout << "Add 0 in the begining" << endl;
	set_a.insert(set_a.begin(), 8);
	printSet(set_a);
	cout << "Cannot add at 8 at 0 place, as Set is ordered" << endl;

	// clear
	cout << "clear called" << endl;
	set_a.clear();
	printSet(set_a);

	// is empty
	cout << "empty() called" << endl;
	cout << "empty()" << set_a.empty() << endl;
}

