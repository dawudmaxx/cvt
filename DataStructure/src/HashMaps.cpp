#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include <algorithm>

using namespace std;

/*
unordered_map is an associated container that stores elements formed by the combination of key-value and a mapped value. 
The key value is used to uniquely identify the element and the mapped value is the content associated with the key. 
Both key and value can be of any type predefined or user-defined.
Internally unordered_map is implemented using Hash Table, the key provided to map are hashed into indices of a hash table 
that is why the performance of data structure depends on hash function a lot but on an average, the cost of search, 
insert and delete from the hash table is O(1).
Note: In the worst case, its time complexity can go from O(1) to O(n2), especially for big prime numbers. 
You can read more about this on how-to-use-unordered_map-efficiently-in-c. 
In this situation, it is highly advisable to use a map instead to avoid getting a TLE error.
*/

void printHashMaps(unordered_map<string, double>& umap)
{
	// unordered_map<string, double>::iterator itr;
	for (auto& map : umap)
	{
		cout << map.first << "  " << map.second << endl;
	}
}

void printFrequencies(const string &str)
{
	// declaring map of <string, int> type, each word
	// is mapped to its frequency
	unordered_map<string, int> wordFreq;

	// breaking input into word using string stream
	stringstream ss(str);  // Used for breaking words
	string word; // To store individual words
	while (ss >> word)
		wordFreq[word]++;

	// now iterating over word, freq pair and printing
	// them in <, > format
	unordered_map<string, int>::iterator p;
	for (p = wordFreq.begin(); p != wordFreq.end(); p++)
		cout << "(" << p->first << ", " << p->second << ")\n";
}

int mainHashMaps()
{
	cout << "<<<<<<<<<< HashMaps (unordered_map) >>>>>>>>>>>>" << endl;
	unordered_map<string, double> umap;

	/*
	at(): returns the ref to the value with the element as the key K
	begin(): returns an iterator pointing to the first element in the unordered_map
	end(): returns an iterator pointing to the last element in the unordered_map
	count(): counts the number of elements present with a given key
	find(): return the iterator of the element, else returns map.end()
	empty(): check wheather container is empty
	erase(): erase element matching the key K
	*/
	// inserting values by using [] operator
	umap["PI"] = 3.14;
	umap["root2"] = 1.414;
	umap["root3"] = 1.732;
	umap["log10"] = 2.302;
	umap["loge"] = 1.0;

	// inserting value by insert function
	umap.insert(make_pair("e", 2.718));

	// print unordered map
	printHashMaps(umap);

	string key = "PI";

	if (umap.find(key) != umap.end())
	{
		cout << key << " " << umap.find(key)->second << endl;
	}

	cout << "Count freq of words in the text " << endl;
	string text = "This  is  the book that this  is not  a book that I like";
	cout << text << endl;
	printFrequencies(text);
	cout << "convert string to lower case" << endl;
	std::for_each(text.begin(), text.end(), [](char & c) {
		c = ::tolower(c);
	});
	printFrequencies(text);

	// remove PI
	cout << "Remove element with key PI" << endl;
	umap.erase("PI");
	printHashMaps(umap);

	// insert again the key loge
	cout << "insert again the key loge" << endl;
	umap.insert(make_pair("loge", 2.0));
	printHashMaps(umap);
	cout << "NOTE: CANNOT insert elemet with same key" << endl;
	cout << "Count element for key loge " << umap.count("loge") << endl;

	return EXIT_SUCCESS;
}