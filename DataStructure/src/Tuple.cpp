#include <tuple>
#include <iostream>
#include <string>

using namespace std;

void printTuple()
{
	cout << "test" << endl;
}

int mainTuple()
{
	tuple<char, int, double, string> tuple_a;

	tuple_a = make_tuple('a', 1, 1.1, "one");

	cout << get<0>(tuple_a) << " " << get<1>(tuple_a) << " " << get<2>(tuple_a) << " " << get<3>(tuple_a) << endl;

	return EXIT_SUCCESS;
}