#include <iostream>
#include <vector>
#include <assert.h>
// #include <xtree>
#include <set>

using namespace std;

struct tree {
	int x;
	//assert(x >= 1);
	//assert(x <= 50000);
	tree * l;
	tree * r;
};

static set<int> container;
static int maximal = 0;

void printArray(int ints[], int len)
{	
	container = *(new set<int>);
	int i;
	for (i = 0; i < len; i++)
	{
		cout << ints[i] << " ";
		container.insert(ints[i]);
	}
	if (container.size() > maximal)
	{
		maximal = container.size();
	}
	cout << endl;
}

void printPathsRecur(tree* node, int path[], int pathLen)
{
	if (node == NULL)
		return;

	/* append this node to the path array */
	path[pathLen] = node->x;
	pathLen++;

	/* it's a leaf, so print the path that lead to here */
	if (node->l == NULL && node->r == NULL)
	{
		printArray(path, pathLen);
	}
	else
	{
		/* otherwise try both subtrees */
		printPathsRecur(node->l, path, pathLen);
		printPathsRecur(node->r, path, pathLen);
	}
}

void printPaths(tree* node)
{
	int path[1000];
	printPathsRecur(node, path, 0);
}

int solution7(tree * T) {
	// write your code in C++14 (g++ 6.2.0)
	printPaths(T);
	if (T != NULL)
	{
		cout << "Max distinct elements = " << maximal << endl;
	}
	else
	{
		cout << "Tree is empty" << endl;
		return -1;
	}
}

int main3()
{
	int N = 10;
	assert(N >= 1);
	assert(N <= 50000);
	tree a;
	int height_of_tree;
	//assert(N >= 0);
	//assert(N <= 3500);

	tree A, B, C, D, E, F, G;
	A.x = 4;
	A.l = &B;
	A.r = &C;
	B.x = 5;
	B.l = &D;
	B.r = NULL;
	D.x = 4;
	D.l = &G;
	D.r = NULL;
	G.x = 5;
	G.l = NULL;
	G.r = NULL;
	A.r = &C;
	C.x = 6;
	C.l = &E;
	C.r = &F;
	E.x = 1;
	F.x = 6;
	E.l = NULL;
	E.r = NULL;
	F.l = NULL;
	F.r = NULL;


	cout << "Ans = " << solution7(&A) << endl;
	return EXIT_SUCCESS;
}