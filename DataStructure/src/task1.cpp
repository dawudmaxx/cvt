#include <vector>
#include <iostream>
#include <assert.h>
#include <set>
#include <iterator>

void printContainer(std::set<int> set)
{
	std::cout << "The set_a is = ";
	for (auto i = set.begin(); i != set.end(); i++)
	{
		std::cout << *i;
	}
	std::cout << std::endl;
}

int solution1(std::vector<int> &T) {
	// write your code in C++14 (g++ 6.2.0)
	std::set<int> container;

	//for (auto i = T.begin(); i != T.end(); i++)
	for(auto i: T)
	{
		container.insert(i);
	}
	printContainer(container);

	int ans = 0;
	int size_ = container.size();
	std::cout << "Size= " << size_ << std::endl;
	// if she has only 1 type of candies
	// if she has < N/2 type of candies
	if (size_ == 1 || size_ < T.size() / 2)
	{
		ans = size_;
	}
	// if she has more than N/2 type of candies
	else
	{
		ans = T.size() / 2;
	}
	std::cout << "Max number of candies Mary can have is = " << ans << std::endl;

	return ans;
}

void check_candy_type(int i)
{
	assert(i >= 1);
	assert(i <= 1000000000);
}

int main2()
{
	int N = 10;

	assert(N % 2 == 0);
	assert(N >= 2);
	assert(N <= 100000);

	std::vector<int> candy_type = { 80, 80, 1000000000, 80, 80, 80, 80, 80, 80, 123456789 };

	/*for (int i = 1; i <= N; i++)
	{
		check_candy_type(i);
		candy_type.push_back(i);
	}*/
	solution1(candy_type);
	return EXIT_SUCCESS;
}

