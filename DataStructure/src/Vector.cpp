#include "Vector.hpp"
/*
Vectors are same as dynamic arrays with the ability to resize itself automatically when an element is inserted or deleted, 
with their storage being handled automatically by the container. Vector elements are placed in contiguous storage so that 
they can be accessed and traversed using iterators. In vectors, data is inserted at the end. Inserting at the end takes 
differential time, as sometimes there may be a need of extending the array. Removing the last element takes only constant 
time because no resizing happens. Inserting and erasing at the beginning or in the middle is linear in time.
*/
void printVec(std::vector<int> vec)
{
	std::cout << "The vec_a is = ";
	for (auto i = vec.begin(); i != vec.end(); ++i)
	{
		std::cout << *i;
	}
	std::cout << std::endl;
}

void runVec()
{
	std::cout << "<<< VECTOR >>> " << std::endl;
	std::vector<int> vec_a;
	vec_a.resize(5);

	// data
	// push_back: object is copied (if copy ctr available) or moved
	// pop
	// begin/end
	// cbegin/cend
	// rbegin/rend
	// rcbegin/rcend
	// at	
	// assign
	// *insert(itr, value): No overriding of data is performed. Inserts a new elemnts before the provided iterator position. 
	//						 object is expensively copied (if copy ctr present) else moved (in C++11).
	// **emplace: prefered over insert. New object is constructed in-place in the container.
	// emplace_back: prefered over push_back. New object is constructed in-place in the container.
	// resize
	// size
	// capacity
	// max_capacity
	for (int i = 1; i <= 5; ++i)
	{
		vec_a.push_back(i);
	}
		
	// Print vec_a
	std::cout << "The vec_a is = ";
	for (auto i = vec_a.begin(); i < vec_a.end(); ++i)
	{
		std::cout << *i;
	}
	std::cout << std::endl;

	// use data to print
	int* a = vec_a.data();
	std::cout << "The vec_a is = ";
	for (int i = 0; i < vec_a.size(); ++i)
	{
		std::cout << *a;
		a++;
	}
	std::cout << std::endl;

	// pop
	std::cout << "pop_back called" << std::endl;
	vec_a.pop_back();
	printVec(vec_a);

	// at
	std::cout << "access data using at() and []" << std::endl;
	std::cout << "The vec_a with at() and [] is = ";
	for (int i = 0; i < vec_a.size(); ++i)
	{
		std::cout << vec_a.at(i);
		// or
		std::cout << vec_a[i];
	}
	std::cout << std::endl;

	// insert => increase the size of the container by 1
	std::cout << "insert at the begining" << std::endl;
	vec_a.insert(vec_a.begin(), 6);
	printVec(vec_a);
	std::cout << "insert at 2nd position" << std::endl;
	vec_a.insert(++vec_a.begin(), 7);
	printVec(vec_a);

	// emplace
	std::cout << "emplace at the begining" << std::endl;
	vec_a.emplace(vec_a.begin(), 8);
	printVec(vec_a);
	std::cout << "emplace at 2nd position" << std::endl;
	vec_a.emplace(++vec_a.begin(), 9);
	printVec(vec_a);

	// size, max_size, capacity
	std::cout << "resize to 6" << std::endl;
	vec_a.resize(6);
	std::cout << "Size = " << vec_a.size() << std::endl;
	std::cout << "Capacity = " << vec_a.capacity() << std::endl;

	// shrink_to_fit 
	std::cout << "shrink_to_fit called" << std::endl;
	vec_a.shrink_to_fit();
	printVec(vec_a);

	// assign
	std::cout << "call assign(5,10)" << std::endl;
	vec_a.assign(5, 10); // assign the vector with 10, 5 times
	printVec(vec_a);

	// vector swap
	std::cout << "swap called" << std::endl;
	std::vector<int> vec_b = {10,20,30};
	vec_a.swap(vec_b);
	printVec(vec_a);
	printVec(vec_b);

	// erase
	std::cout << "erase called for .begin() itr" << std::endl;
	vec_a.erase(vec_a.begin());
	printVec(vec_a);
}