#include <iostream>
#include "Map.hpp"
#include <iterator>
#include "Set.hpp"
#include <vector>

/*
Maps are associative containers that store elements in a mapped fashion. Each element has a key value and a mapped value.
No two mapped values can have the same key values.
The map stores the entries in sorted order of key.
*/
using namespace std;

void printMap(map<int, int> &map)
{
	for (auto itr = map.begin(); itr != map.end(); itr++)
	{
		cout << "key(" << itr->first << ") => value(" << itr->second<<")"<< endl;
	}
}

void runMap()
{
	cout << "<<< MAP >>> " << endl;
	map<int, int> map_a = {{1,10}, {2,20}, {3,30} };

	for (int i = 1; i <= 3; i++)
	{
		map_a.insert(pair<int, int>(3+i, 30+i*10));
	}
	printMap(map_a);

	// find, itr points to map.end() if not found
	map<int, int>::iterator itr = map_a.find(5);
	cout << "FIND 5" << endl;
	cout << "key(" << itr->first << ") => value(" << itr->second << ")" << endl;

	itr = map_a.find(11);
	cout << "FIND 11" << endl;
	if (itr == map_a.end())
	{
		cout << "not found" << endl;
	}

	// count keys with 1
	if (map_a.count(1))
	{
		cout << "key 1 present (" << map_a.count(1) << ") times" << endl;
	}
	else
	{
		cout << "key 1 not present " << endl;
	}

	// erase 5
	// map<int, int>::iterator itr = map_a.find(5);
	cout << "Erase 5" << endl;
	map_a.erase(5);
	printMap(map_a);

	// add in the begining
	cout << "Add at key:5 at pos:0" << endl;
	map_a.insert(map_a.begin(), pair<int, int>(5, 50));
	printMap(map_a);

	// chaneg item at pos:0
	cout << "Change the key at 0" << endl;
	// map_a.begin()->first = 7;
	cout << "Cannot change key at pos:0" << endl;

	// remove all
	cout << "Remove all" << endl;
	map_a.clear();
	printMap(map_a);

}

struct node
{
	int element;
	node* left;
	node* right;
}typedef node;
static int counter = 0;
void printTree(node *root, vector<int> &vec)
{
	if (root == NULL)
	{
		return;
	}
	else
	{
		vec.push_back(root->element);
	}
	printTree(root->left, vec);
	printTree(root->right, vec);

	// print tree path and pop_back the node
	// check if root is a leaf node
	if (root->left == NULL && root->right == NULL)
	{
		cout << "Path = " << counter << endl;
		counter++;
		for (auto i = 0; i < vec.size(); i++)
		{
			cout << vec[i] << " ";
		}
		cout << endl;
	}
	
	vec.pop_back();
}

int mainMap()
{
	runMap();
	/*
	A=1(l=>B,r=>C)
		B=2(l=>D,r=>NULL)
		C=3(l=>NULL,r=>E)
			D=4(l=r=NULL)
			E=5(l=r=NULL)
	*/
	node A, B, C, D, E;
	A.element = 1; A.left = &B; A.right = &C;
	B.element = 2; B.left = &D; B.right = NULL;
	C.element = 3; C.left = NULL; C.right = &E;
	D.element = 4; D.left = NULL; D.right = NULL;
	E.element = 5; E.left = NULL; E.right = NULL;
	vector<int> vec;
	printTree(&A, vec);

	return EXIT_SUCCESS;
}