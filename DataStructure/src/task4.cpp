#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

int solution(int A, int B) {
	// write your code in C++14 (g++ 6.2.0)
	int cnt = 0;
	for (int i = A; i <= B; i++)

		// Check if current number 'i' is perfect square
		for (int j = 1; j * j <= i; j++)
			if (j * j == i)
				cnt++;
	return cnt;
}


int main4()
{
	cout << solution(4, 7);
	return EXIT_SUCCESS;
}