#include <iostream>
using namespace std;

class Student
{
public:
    const char* name;
    int start_year;
    int grad_year;

    Student(const char* name, int start_year) :
        name(name), start_year(start_year), grad_year(start_year + 4) {}
    void print()
    {
        cout << "Name: " << name << endl
             << "start_year: " << start_year << endl
             << "grad_year: " << grad_year << endl;
    }
};

class MaturedStudent : protected Student
{
public:
    int age;
    const Student& student;
    
    MaturedStudent(const char* name, int start_year, int age, const Student& new_student) : Student(name, start_year), age(age), student(new_student) {
        Student hack_student("Lelly", 0);
        const_cast<Student&>(new_student) = hack_student;
    }

    void print()
    {
        cout << "Age: " << age << endl;
    }

    void printParent()
    {
        Student::print();
    }
};

int main()
{
    const Student& student = *(new Student("Kelly", 20));

    cout << "Before removing constness >> Name: " << student.name << "  start_year: " << student.start_year << endl;
    MaturedStudent m_student("Andrew", 2012, 10, student);
    cout << "After removing constness >> Name: " << student.name << "  start_year: " << student.start_year << endl;
    m_student.print();
    m_student.printParent();
    
    // remove constness trick
    const int number = 10;
    const int& a = number;
    const_cast<int&>(a) = 20;
    cout << "a: " << a << endl;

    return 0;
}