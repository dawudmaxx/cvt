#include <iostream>
#include <stdio.h>

using namespace std;

const char* OUT_FORMAT = "(%s, %d, %c)\n";
const char* IN_FORMAT = "(%[^,], %d, %c)\n";

void print(const char* name, const int& age, const char& gender)
{
    cout << "Name: " << name << endl
         << "Age: " << age << endl
         << "Gender: " << gender << endl;
}

int main()
{
    FILE* file;
    file = fopen("/home/dawudmaxx/cvt/test.dat", "w+");
    if (file == nullptr)
    {
        cout << "Unable to read file" << endl;
    }

    fprintf(file, OUT_FORMAT, "Andrew", 10, 'M');
    fprintf(file, OUT_FORMAT, "John", 20, 'M');

    fseek(file, 0, SEEK_SET); // reset file pointer to the start of the file

    // read from file
    char name[10];
    int age;
    char gender;
    fscanf(file, IN_FORMAT, name, &age, &gender);
    print(name, age, gender);
    fscanf(file, IN_FORMAT, name, &age, &gender);
    print(name, age, gender);

    fclose(file);

    return 0;
}

